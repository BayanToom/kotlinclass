import java.util.*

interface myInterface {
    fun create(name: String)
    fun update(name: String, name2: String)
    fun delete(name: String)
    fun find(name: String): String
    fun findAll(name: String): Any

}