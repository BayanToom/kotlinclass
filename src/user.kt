import java.util.*

class user : myInterface {
    var users: ArrayList<String> = ArrayList<String>()

    init {
        users.add("Bayan")
        users.add("Weam")
        users.add("Yaqeen")

    }


    override fun create(name: String) {
        users.add(name)
    }

    override fun delete(name: String) {
        users.remove(name)
    }

    override fun update(name: String, name2: String) {
        users.replaceAll { name -> name2 }
    }

    override fun find(name: String): String {
        if (users.contains(name))
            return name
        else
            return "not found"
    }

    override fun findAll(name: String): Any {
        if (users.contains(name))
            return users
        else
            return "not found"
    }
}